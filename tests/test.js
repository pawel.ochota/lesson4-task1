const fillElement = require('..');
const { toBeEmpty, toHaveTextContent } = require('@testing-library/jest-dom/matchers');

expect.extend({ toBeEmpty, toHaveTextContent });

afterEach(() => {
  document.body.innerHTML = '';
});

describe('fillElement', () => {
  it('should returns a HTMLElement', async () => {
    const result = fillElement('test');

    expect(result instanceof HTMLElement).toBeTruthy();
  });

  it('should returns a HTMLElement which is not empty', async () => {
    const span = fillElement('test');
    document.body.innerHTML += `
      <div id="test"></div>
    `;
    document.querySelector('#test').appendChild(span);
    const result = document.querySelector('#test > span');

    if (result.innerText) {
      expect(result.innerText).not.toEqual('');
    } else {
      expect(result).not.toBeEmpty();
    }
  });

  it('should returns a HTMLElement with text from function argument', async () => {
    const span = fillElement('Ala ma kota');
    document.body.innerHTML += `
      <div id="test"></div>
    `;
    document.querySelector('#test').appendChild(span);
    const result = document.querySelector('#test > span');

    if (result.innerText) {
      expect(result.innerText).toEqual('Ala ma kota');
    } else {
      expect(result).toHaveTextContent('Ala ma kota');
    }
  });
});
